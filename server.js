var express = require('express');
var less = require('less-middleware');
var mongoose = require('mongoose');
var morgan = require('morgan');
var bodyParser = require('body-parser');

var app = express();
var routes = require('./api/route_index');

mongoose.connect('mongodb://localhost/pjatk-tin');


app.use(bodyParser.json());
app.use(morgan(':method :url :status'));
app.use(express.static(__dirname + '/public'));
app.use(less(__dirname + "/public"));
app.use('/bower_components', express.static(__dirname + '/bower_components'));
app.use('/api', routes);



app.get('/', function(req, res) {
  return res.sendFile(__dirname + '/view/index.html');
});

var ports = process.env.PORT || 3000;

app.listen(ports);

console.log('Serwer dziala na porcie', ports);
