var mongoose = require('mongoose');

var positionsSchema = {
  name: {type: String},
  salary: {type: Number, defaultValue: 0.0}
};

module.exports = mongoose.model('Position', positionsSchema);
