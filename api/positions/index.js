var router = require('express').Router();
var PositionsController = require('./positions_controller');

router.get('/', PositionsController.index);
router.get('/:id', PositionsController.show);
router.post('/', PositionsController.create);
router.put('/:id', PositionsController.update);
router.patch('/:id', PositionsController.update);
router.delete('/:id', PositionsController.destroy);

module.exports = router;
