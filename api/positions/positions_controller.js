var Position = require('./positions_model');
var utils = require('../utils');
var errHandler = utils.errHandler;
var _ = utils._;

var validPositionFilter = function(position) {
  if (position.name.trim() !== '' && typeof(position.salary) === 'number') {
    return position;
  } else {
    console.log("BŁĄD: Pozycja o ObjectID: " + position._id + " posiada niepoprawne wartoście");
  }  
};


exports.index = function(req, res) {
  Position.find().select('-__v').exec(function(err, positions) {
    if (err) { return errHandler(res, err); }
    return res.status(200).json(positions.filter(validPositionFilter));
  });
};

exports.show = function(req, res) {
  Position.findById(req.params.id, function(err, position) {
    if (err) { return errHandler(res, err); }
    if (!position) { return res.sendStatus(404); }
    if (!validPositionFilter(position)) { 
      return res.sendStatus(404);
    }
    return res.status(200).json(position);
  });
};

exports.create = function(req, res) {
  Position.create(req.body, function(err, position) {
    if (err) { return errHandler(res, err); }
    return res.status(201).json(position);
  });
};

exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Position.findById(req.params.id, function(err, position) {
    if (err) { return errHandler(res, err); }
    if (!position) { return res.sendStatus(404); }
    var updated = _.merge(position, req.body);
    updated.save(function(err) {
      if (err) { return errHandler(res, err); }
      return res.status(200).json(position);
    });
  });
};

exports.destroy = function(req, res) {
  Position.findById(req.params.id, function(err, position) {
    if (err) { return errHandler(res, err); }
    if (!position) { return res.sendStatus(404); }
    position.remove(function(err) {
      if (err) { return errHandler(res, err); }
      return res.sendStatus(204);
    });
  });
};
