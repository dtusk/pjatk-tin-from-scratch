var Worker = require('./workers_model');
var utils = require('../utils');
var errHandler = utils.errHandler;
var _ = utils._;

exports.index = function(req, res) {
  var query = {};
  if (req.query.status) {
    query.status = req.query.status;
  }
  if (req.query.notInTeam) {
    query.team = null;
  } else if (req.query.team) {
    query.team = req.query.team;
  }
  Worker.find(query).populate('position').select('-__v').exec(function(err, workers) {
    if (err) { return errHandler(res, err); }
    var validWorkers = [];
    workers.filter(function(worker) {
      if (worker.firstName !== '' && worker.lastName !== '' && worker.dateOfHiring !== '') {
        validWorkers.push(worker);
      } else {
        console.log("BŁĄD: Pracownik o ObjectId " + worker._id + " posiada niepoprawne wartoście"); 
      }
    });
    return res.status(200).json(validWorkers);
  });
};

exports.show = function(req, res) {
  Worker.findById(req.params.id).populate('position').select('-__v').exec(function(err, worker) {
    if (err) { return errHandler(res, err); }
    if (!worker) { return res.sendStatus(404); }
    return res.status(200).json(worker);
  });
};

exports.create = function(req, res) {
  Worker.create(req.body, function(err, worker) {
    if (err) { return errHandler(res, err); }
    return res.status(201).json(worker);
  });
};

exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Worker.findById(req.params.id, function(err, worker) {
    if (err) { return errHandler(res, err); }
    if (!worker) { return res.sendStatus(404); }
    var updated = _.merge(worker, req.body);
    updated.save(function(err) {
      if (err) { return errHandler(res, err); }
      return res.status(200).json(worker);
    });
  });
};

exports.destroy = function(req, res) {
  Worker.findById(req.params.id, function(err, worker) {
    if (err) { return errHandler(res, err); }
    if (!worker) { return res.sendStatus(404); }
    worker.remove(function(err) {
      if (err) { return errHandler(res, err); }
      return res.sendStatus(204);
    });
  });
};
