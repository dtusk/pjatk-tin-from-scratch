var router = require('express').Router();
var WorkersController = require('./workers_controller');

router.get('/', WorkersController.index);
router.get('/:id', WorkersController.show);
router.post('/', WorkersController.create);
router.put('/:id', WorkersController.update);
router.patch('/:id', WorkersController.update);
router.delete('/:id', WorkersController.destroy);

module.exports = router;
