var mongoose = require('mongoose');

var workersSchema = {
  firstName: {type: String, required: true},
  lastName: {type: String, required: true},
  dateOfHiring: {type: String, required: true},
  dateOfBirth: {type: String, required: true},
  position: {type: mongoose.Schema.Types.ObjectId, ref: 'Position'},
  team: {type: mongoose.Schema.Types.ObjectId, ref: 'Team'},
  status: {type: String, required: true}
};

module.exports = mongoose.model('Worker', workersSchema);
