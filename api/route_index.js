var router = require('express').Router();

var positionsRoute = require('./positions/index');
var workersRoute = require('./workers/index');
var teamsRoute = require('./teams/index');

router.use('/positions', positionsRoute);
router.use('/workers', workersRoute);
router.use('/teams', teamsRoute);

module.exports = router;
