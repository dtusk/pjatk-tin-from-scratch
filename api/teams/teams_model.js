var mongoose = require('mongoose');

var teamsSchema = {
  name: {type: String, required: true}
};

module.exports = mongoose.model('Team', teamsSchema);
