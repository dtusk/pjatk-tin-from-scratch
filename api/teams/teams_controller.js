var Team = require('./teams_model');
var Worker = require('../workers/workers_model');
var utils = require('../utils');
var errHandler = utils.errHandler;
var _ = utils._;

exports.index = function(req, res) {
  Team.find().select('-__v').exec(function(err, teams) {
    if (err) { return errHandler(res, err); }
    return res.status(200).json(teams);
  });
};

exports.show = function(req, res) {
  Team.findById(req.params.id).select('-__v').exec(function(err, team) {
    if (err) { return errHandler(res, err); }
    if (!team) { return res.sendStatus(404); }
    return res.status(200).json(team);
  });
};

exports.create = function(req, res) {
  var workerId = [];
  if (req.body.workers) {
    req.body.workers.filter(function(worker) {
      workerId.push(worker._id);
    });
    delete req.body.workers;
  }
  Team.create(req.body, function(err, team) {
    if(err) { return errHandler(res, err); }
    workerId.filter(function(id) {
      Worker.findById(id, function(err, worker) {
        worker.team = team._id;
        worker.save(function(err) {
          if (err) { return errHandler(res, err); }
        });
      });
    });
    return res.status(201).json(team);
  });
};

exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  var workerId = [];
  if (req.body.workers) {
    req.body.workers.filter(function(worker) {
      workerId.push(worker._id);
    });
    // this is to erase the rights to the team
    Worker.update({team: req.params.id}, {team: null}, function(err, worker) {
      if (err) { return errHandler(res, err); }
    });
    delete req.body.workers;
  }
  Team.findById(req.params.id, function(err, team) {
    if (err) { return errHandler(res, err); }
    if (!team) { return res.sendStatus(404); }
    var updated = _.merge(team, req.body);
    updated.save(function(err) {
      if (err) { return errHandler(res, err); }
      workerId.filter(function(id) {
        Worker.findById(id, function(err, worker) {
          worker.team = team._id;
          worker.save(function(err) {
            if (err) { return errHandler(res, err); }
          });
        });
      });

      return res.status(200).json(team);
    });
  });
};

exports.destroy = function(req, res) {
  Team.findById(req.params.id, function(err, team) {
    if (err) { return errHandler(res, err); }
    if (!team) { return res.sendStatus(404); }
    team.remove(function(err) {
      if (err) { return errHandler(res, err); }
      return res.sendStatus(204);
    });
  });
};
