var router = require('express').Router();
var TeamsController = require('./teams_controller');

router.get('/', TeamsController.index);
router.get('/:id', TeamsController.show);
router.post('/', TeamsController.create);
router.put('/:id', TeamsController.update);
router.patch('/:id', TeamsController.update);
router.delete('/:id', TeamsController.destroy);

module.exports = router;
