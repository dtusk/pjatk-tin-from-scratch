exports._ = require('lodash');
exports.errHandler = function (res, err) {
  return res.send(500, err);
};
