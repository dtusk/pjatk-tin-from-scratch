var app = angular.module('app', ['ngRoute', 'ngCookies', 'ngDragDrop']);

app.controller('HomeController', function($route, $scope, $cookies, $location) {
  $scope.name = $cookies.auth;
  $scope.doLogout = function() {
    if ($cookies.auth) {
      delete $cookies.auth;
      $route.reload();
    }
  };
});

// Position

app.controller('PositionsController', function($scope, $http) {
  $scope.positions = [];
  $http.get('/api/positions').success(function(positions) {
    $scope.positions = positions;
  });
});

app.controller('PositionsNewController', function($scope, $http, $location) {
  $scope.operation = "Nowy";
  $scope.addPosition = function(form) {
    $scope.submitted = true;
    if (form.$valid) {
      $http.post('/api/positions', $scope.fields).success(function(position) {
        $scope.fields = {};
        $location.path('positions/' + position._id);
      });
    } else {
      $scope.response = "Są błędy. Sprawdź formularz";
    }
  };
});

app.controller('PositionsShowController', function($scope, $location, $routeParams, $http) {
  $scope.position = null;
  $http.get('/api/positions/' + $routeParams.id).success(function(position) {
    $scope.position = position;
  });

  $scope.deletePosition = function() {
    $http.delete('/api/positions/' + $routeParams.id).success(function() {
      $location.path('/positions');
    });
  };
});

app.controller('PositionsEditController', function($scope, $location, $routeParams, $http) {
  $scope.operation = "Edytuj";
  $scope.fields = null;
  $http.get('/api/positions/' + $routeParams.id).success(function(position) {
    $scope.fields = position;
  });

  $scope.addPosition = function(form) {
    $scope.submitted = true;
    if (form.$valid) {
      $http.put('/api/positions/' + $routeParams.id, $scope.fields).success(function(position) {
        $scope.fields = {};
        $location.path('/positions/' + position._id);
      });
    } else {
      $scope.response = "Są błędy. Sprawdź formularz";
    }
  };
});

app.controller('WorkersController', function ($scope, $http) {
  $scope.workers = [];
  $http.get('/api/workers').success(function(workers) {
    $scope.workers = workers;
  });
});

app.controller('WorkersNewController', function($scope, $http, $location) {
  $scope.operation = 'Nowy';
  $scope.positions = [];
  $http.get('/api/positions').success(function(positions) {
    $scope.positions = positions;
  });
  $scope.addWorker = function(form) {
    $scope.submitted = true;
    if (form.$valid) {
      $scope.fields.position = $scope.fields.position._id;
      $http.post('/api/workers', $scope.fields).success(function(worker) {
        $scope.fields = {};
        $location.path('workers/' + worker._id);
      });
    } else {
      $scope.response = "Są błędy. Sprawdź formularz";
    }
  };
});

app.controller('WorkersShowController', function($scope, $location, $routeParams, $http) {
  $scope.worker = null;
  $scope.position = null;
  
  $http.get('/api/workers/' + $routeParams.id).success(function(worker) {
    $scope.worker = worker;
  });

  $scope.deleteWorker = function() {
    $http.delete('/api/workers/' + $routeParams.id).success(function() {
      $location.path('/workers');
    });
  };
});

app.controller('WorkersEditController', function($scope, $location, $routeParams, $http) {
  $scope.fields = null;
  $scope.operation = "Edytuj";
  $http.get('/api/workers/' + $routeParams.id).success(function(worker) {
    $scope.fields = worker;
    $scope.fields.dateOfHiring = new Date($scope.fields.dateOfHiring);
    $scope.fields.dateOfBirth = new Date($scope.fields.dateOfBirth);
  });

  $http.get('/api/positions').success(function(positions) {
    $scope.positions = positions;
  });


  $scope.addWorker = function() {
    $scope.submitted = true;
    $scope.fields.position = $scope.fields.position._id;
    $http.put('/api/workers/' + $routeParams.id, $scope.fields).success(function(worker) {
      $scope.fields = {};
      $location.path('/workers/' + worker._id);
    });
  };
});

app.controller('TeamsController', function ($scope, $http) {
  $scope.teams = [];
  $http.get('/api/teams').success(function(teams) {
    $scope.teams = teams;
    teams.filter(function(team) {
      $http.get('/api/workers?team=' + team._id).success(function(workers) {
        team.workers = workers;
      });
    });
  });
});

app.controller('TeamsNewController', function ($scope, $http, $location) {
  $scope.availableWorkers = [];
  $scope.selectedWorkers = [];
  $http.get('/api/workers?status=Zatrudniony&notInTeam=1').success(function(workers) {
    workers.filter(function(worker) {
      worker.drag = true;
      return worker;
    });
    $scope.availableWorkers = workers;
  });

  $scope.addTeam = function(form) {
    $scope.submitted = true;
    if (form.$valid) {
      $scope.selectedWorkers.map(function(worker) {
        return worker._id;
      }); 
      console.log($scope.selectedWorkers);
      $scope.fields.workers = $scope.selectedWorkers;
      $http.post('/api/teams', $scope.fields).success(function(team) {
        $scope.fields = {};
        $location.path('teams/' + team._id);
      });
    } else {
      $scope.response = "Są błędy. Sprawdź formularz";
    }
  };
});

app.controller('TeamsShowController', function($scope, $routeParams, $http, $location) {
  $scope.team = null;
  $http.get('/api/teams/' + $routeParams.id).success(function(team) {
    $scope.team = team;
    $http.get('/api/workers?team=' + team._id).success(function(workers) {
      $scope.team.workers = workers;
    });
  });
  $scope.deleteTeam = function() {
    $http.delete('/api/teams/' + $scope.team._id);
    $location.path('teams');
  };
});

app.controller('TeamsEditController', function($scope, $routeParams, $http, $location) {
  $scope.availableWorkers = [];
  $scope.selectedWorkers = [];
  $scope.fields = {};
  $http.get('/api/teams/' + $routeParams.id).success(function(team) {
    $scope.fields = team;
  });
  $http.get('/api/workers?team=' + $routeParams.id).success(function(workers) {
    workers.filter(function(worker) {
      worker.drag = true;
      return worker;
    });
    $scope.selectedWorkers = workers;
  });
  $http.get('/api/workers?status=Zatrudniony&notInTeam=1').success(function(workers) {
    workers.filter(function(worker) {
      worker.drag = true;
      return worker;
    });
    $scope.availableWorkers = workers;
  });

  $scope.addTeam = function(form) {
    $scope.submitted = true;
    if (form.$valid) {
      $scope.selectedWorkers.map(function(worker) {
        return worker._id;
      }); 
      console.log($scope.selectedWorkers);
      $scope.fields.workers = $scope.selectedWorkers;
      $http.put('/api/teams/' + $routeParams.id, $scope.fields).success(function(team) {
        $scope.fields = {};
        $location.path('teams/' + team._id);
      });
    } else {
      $scope.response = "Są błedy. Sprawdź formularz";
    }
  };
});

app.controller('LoginController', function($scope, $http, $cookies, $location) {

  var loginCounter = 0;
  $scope.doLogin = function() {
    if ($scope.fields.login === "dtusk" && $scope.fields.passwd === "123") {
      $cookies.auth = $scope.fields.login;
      $location.path('/');
    } else {
      console.log(JSON.stringify($scope.fields));
      loginCounter = loginCounter + 1;
      $scope.response = "Nie zalogowano się (" + loginCounter + " razy)";
      $location.path('/login');
    }
  };
});

app.config(function($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'js/templates/home.html',
      controller: 'HomeController'
    })
    .when('/positions', {
      templateUrl: 'js/templates/positions/index.html',
      controller: 'PositionsController'
    })
    .when('/positions/new', {
      templateUrl: 'js/templates/positions/new.html',
      controller: 'PositionsNewController'
    })
    .when('/positions/:id/edit', {
      templateUrl: 'js/templates/positions/new.html',
      controller: 'PositionsEditController'
    })
   .when('/positions/:id', {
      templateUrl: 'js/templates/positions/show.html',
      controller: 'PositionsShowController'
    })
    .when('/workers', {
      templateUrl: 'js/templates/workers/index.html',
      controller: 'WorkersController'
    })
    .when('/workers/new', {
      templateUrl: 'js/templates/workers/new.html',
      controller: 'WorkersNewController'
    })
    .when('/workers/:id', {
      templateUrl: 'js/templates/workers/show.html',
      controller: 'WorkersShowController'
    })
    .when('/workers/:id/edit', {
      templateUrl: 'js/templates/workers/new.html',
      controller: 'WorkersEditController'
    })
    .when('/teams', {
      templateUrl: 'js/templates/teams/index.html',
      controller: 'TeamsController'
    })
    .when('/teams/new', {
      templateUrl: 'js/templates/teams/new.html',
      controller: 'TeamsNewController'
    })
    .when('/teams/:id', {
      templateUrl: 'js/templates/teams/show.html',
      controller: 'TeamsShowController'
    })
    .when('/teams/:id/edit', {
      templateUrl: 'js/templates/teams/new.html',
      controller: 'TeamsEditController'
    })
    .when('/login', {
      templateUrl: 'js/templates/login.html',
      controller: 'LoginController'
    })
    .otherwise({
      redirectTo: '/'
    });
});

app.run(function($location, $rootScope, $cookies) {
  $rootScope.$on('$routeChangeStart', function (event, current, previous) {
    if (!$cookies.auth && $location.path() !== "/login") {
      event.preventDefault();
      $location.path('/login');
    } else {
      $rootScope.loggedIn = true;
    }
  });
});
